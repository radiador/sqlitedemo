//
//  DataManager.swift
//  SqliteDemo
//
//  Created by formador on 29/4/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation
import GRDB

class DataManager {
    
    static let dataManager = DataManager()
    
    private init() {}
    
    var dbQueue: DatabaseQueue?

    func setupDataBase() {
        
        do {
            try openDB()
            
            guard let dbQueue = dbQueue else {
                return
            }
            
            //SQLITE
            try dbQueue.write({ db in
                
                try db.create(table: "student", temporary: false, ifNotExists: true, body: { t in
                    t.autoIncrementedPrimaryKey("id")
                    t.column("name")
                    t.column("surname")
                })

                
                try db.create(table: "school", temporary: false, ifNotExists: true, body: { t in
                    t.autoIncrementedPrimaryKey("id")
                    t.column("name")
                    t.column("address")
                })
                
            })
            
        }catch {
            print("Error al intentar crear la tabla Student \(error)")
        }
    }
    
    func loadStudents() -> [Student] {
        
        guard let dbQueue = dbQueue else {
            return [Student]()
        }
        
        do {
           return try dbQueue.read({ db -> [Student] in
                try Student.fetchAll(db, "select * from student")
            })
            
        } catch {
            print("Error al intentar recuperar la tabla Student \(error)")
        }
        
        return [Student]()
    }
    
    func addStudent(_ student: Student) {
        
        guard let dbQueue = dbQueue else {
            return
        }
        
        do {
            try dbQueue.write { db in
                try student.insert(db)
            }
        } catch {
            print("Error al intentar guardar la tabla Student \(error)")
        }
    }
    
    private func openDB() throws  {
        
        let applicationSupportFolderURL = try FileManager.default.url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let dbURL = applicationSupportFolderURL.appendingPathComponent("demoDB.sqlite")
        
        dbQueue = try DatabaseQueue(path: dbURL.path)
    }
    
}
