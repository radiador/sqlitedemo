//
//  Student.swift
//  SqliteDemo
//
//  Created by formador on 29/4/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation
import GRDB

struct Student {
    let name: String
    let surname: String
}


extension Student: FetchableRecord, PersistableRecord {
    init(row: Row) {
        name = row["name"]
        surname = row["surname"]
    }
    
    func encode(to container: inout PersistenceContainer) {
        container["name"] = name
        container["surname"] = surname
    }
}



