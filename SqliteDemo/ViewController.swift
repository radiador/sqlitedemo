//
//  ViewController.swift
//  SqliteDemo
//
//  Created by formador on 25/4/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var studentsSearchBar: UISearchBar!
    @IBOutlet weak var studentNameTextField: UITextField!
    @IBOutlet weak var studentSurnameTextField: UITextField!
    @IBOutlet weak var studentsTableView: UITableView!
    
    private var students = [Student]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        students = DataManager.dataManager.loadStudents()
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        
    }
    
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "", for: indexPath)
        
        let student = students[indexPath.row]
        
        cell.textLabel?.text = student.name
        cell.detailTextLabel?.text = student.surname
        
        return cell
    }
}

